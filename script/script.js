function Mathfuc(a, b, c) {
    switch (c) {
        case "*":
            return a * b;
        case "/":
            return a / b;
        case "+":
            return +a + +b;
        case "-":
            return a - b;
    }
}

let num1 = "first number";
let num2 = "second number";

while (!Number.isInteger(+num1) || !Number.isInteger(+num2)) {
    num1 = prompt("Enter number 1", num1);
    num2 = prompt("Enter number 2", num2);
}

let oper = prompt("Enter sign");

alert(Mathfuc(num1, num2, oper));